import { Empleado } from "@models/Empleado";
import { RtaApi } from "@models/RtaApi";

export const mockRtaEditarEmpleado:RtaApi<Empleado> = {
    "bRta": true,
    "sMsg": "Empleado actualizado correctamente",
    "lista": [
        {
            "codEmpleado": "26",
            "primerApellido": "BAQUERO",
            "segundoApellido": "NN",
            "primerNombre": "NICOLAS",
            "otrosNombres": "",
            "numeroIdentificacion": "c10208357s16",
            "correoElectronico": "nicolas.baquero@cidenet.com.co",
            "fechaIngreso": "2021-10-15T05:00:00.000+00:00",
            "estado": "ACT",
            "fechaRegistro": "2021-10-15T23:56:43.000+00:00",
            "fechaModificacion": "2021-10-16T00:02:50.554+00:00",
            "codPais": {
                "codPais": 1,
                "nombre": "Colombia"
            },
            "codTipoIdentificacion": {
                "codTipoIdentificacion": 1,
                "nombre": "Cédula de Ciudadanía"
            },
            "codArea": {
                "codArea": 2,
                "nombre": "Financiera"
            }
        }
    ]
};

export const mockBodyEditarEmpleado = {
    "primerApellido":"BAQUERO",
    "segundoApellido":"NN",
    "primerNombre":"NICOLAS",
    "otrosNombres":"",
    "paisEmpleo":1,
    "tipoIdentificacion":1,
    "numeroIdentificacion":"c10208357s16",
    "estado":"ACT",
    "area":2
}