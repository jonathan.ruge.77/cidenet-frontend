export const mockSelectTipoIdent = {
    bRta: true,
    sMsg: "Tipos de identificación encontrados",
    lista: [
        {
            "codTipoIdentificacion": 1,
            "nombre": "Cédula de Ciudadanía"
        },
        {
            "codTipoIdentificacion": 2,
            "nombre": "Cédula de Extranjería"
        },
        {
            "codTipoIdentificacion": 3,
            "nombre": "Pasaporte"
        },
        {
            "codTipoIdentificacion": 4,
            "nombre": "Permiso Especial"
        }
    ]
};

export const mockSelectArea = {
    "bRta": true,
    "sMsg": "Áreas encontradas",
    "lista": [
        {
            "codArea": 1,
            "nombre": "Administración"
        },
        {
            "codArea": 2,
            "nombre": "Financiera"
        },
        {
            "codArea": 3,
            "nombre": "Compras"
        },
        {
            "codArea": 4,
            "nombre": "Infraestructura"
        },
        {
            "codArea": 5,
            "nombre": "Operación"
        },
        {
            "codArea": 6,
            "nombre": "Talento Humano"
        },
        {
            "codArea": 7,
            "nombre": "Servicios Varios"
        },
        {
            "codArea": 8,
            "nombre": "Otra"
        }
    ]
}

export const mockSelectPais = {
    "bRta": true,
    "sMsg": "Países encontradas",
    "lista": [
        {
            "codPais": 1,
            "nombre": "Colombia"
        },
        {
            "codPais": 2,
            "nombre": "Estados unidos"
        }
    ]
}