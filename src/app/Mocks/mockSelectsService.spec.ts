import { Injectable } from "@angular/core";
import { RtaApi } from "@models/RtaApi";
import { Area, Pais, TipoIdentificacion } from "@models/Selects";
import { Observable, Observer } from "rxjs";
import { mockSelectArea, mockSelectPais, mockSelectTipoIdent } from "./Selects.spec";

@Injectable({
    providedIn: 'root'
})
export class MockSelects {
    obtenerTiposIdentificacion() {
        return new Observable((observer: Observer<RtaApi<TipoIdentificacion>>) => {
            observer.next(mockSelectTipoIdent);
            observer.complete();
        });
    }

    obtenerPaises() {
        return new Observable((observer: Observer<RtaApi<Pais>>) => {
            observer.next(mockSelectPais);
            observer.complete();
        });
    }

    obtenerAreas() {
        return new Observable((observer: Observer<RtaApi<Area>>) => {
            observer.next(mockSelectArea);
            observer.complete();
        });
    }

}