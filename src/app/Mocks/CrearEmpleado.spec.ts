import { Empleado } from "@models/Empleado";
import { RtaApi } from "@models/RtaApi";

export const mockBodyCrearEmpleado = {
    "primerApellido": "GARCIA",
    "segundoApellido": "CELIS",
    "primerNombre": "LUZ",
    "otrosNombres": "",
    "paisEmpleo": 1,
    "tipoIdentificacion": 1,
    "numeroIdentificacion": "52323139322",
    "fechaIngreso": "2021-10-15",
    "area": 1
};

export const mockRtaCrearEmpleado:RtaApi<Empleado> = {
    "bRta": true,
    "sMsg": "Empleado creado correctamente",
    "lista": [
        {
            "codEmpleado": "26",
            "primerApellido": "GARCIA",
            "segundoApellido": "CELIS",
            "primerNombre": "LUZ",
            "otrosNombres": "",
            "numeroIdentificacion": "52323139322",
            "correoElectronico": "luz.garcia@cidenet.com.co",
            "fechaIngreso": "2021-10-15T05:00:00.000+00:00",
            "estado": "ACT",
            "fechaRegistro": "2021-10-15T23:56:43.700+00:00",
            "fechaModificacion": "2021-10-15T23:56:43.700+00:00",
            "codPais": {
                "codPais": 1,
                "nombre": "Colombia"
            },
            "codTipoIdentificacion": {
                "codTipoIdentificacion": 1,
                "nombre": "Cédula de Ciudadanía"
            },
            "codArea": {
                "codArea": 1,
                "nombre": "Administración"
            }
        }
    ]
};