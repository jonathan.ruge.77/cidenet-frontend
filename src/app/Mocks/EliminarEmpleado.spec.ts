import { Empleado } from "@models/Empleado";
import { RtaApi } from "@models/RtaApi";

export const mockEliminarEmpleado:RtaApi<Empleado> = {
    "bRta": true,
    "sMsg": "Se ha eliminado el empleado de manera correcta"
};