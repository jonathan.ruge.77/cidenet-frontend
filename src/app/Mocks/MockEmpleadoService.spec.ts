import { Injectable } from "@angular/core";
import { Empleado } from "@models/Empleado";
import { RtaApi } from "@models/RtaApi";
import { EmpleadoService } from "@services/empleado/empleado.service";
import { Observable, Observer } from "rxjs";
import { mockBuscarFiltrado, mockBuscarPaginado } from "./BuscarEmpleado.spec";
import { mockRtaCrearEmpleado } from "./CrearEmpleado.spec";
import { mockRtaEditarEmpleado } from "./EditarEmpleado.spec";
import { mockEliminarEmpleado } from "./EliminarEmpleado.spec";

@Injectable({
    providedIn: 'root'
})
export class MockEmpleado  {
    crearEmpleado(jsonEmpleado: string) {
        return new Observable((observer: Observer<RtaApi<Empleado>>) => {
            observer.next(mockRtaCrearEmpleado);
            observer.complete();
        });
    }

    editarEmpleado(jsonEmpleado: string, id: string) {
        return new Observable((observer: Observer<RtaApi<Empleado>>) => {
            observer.next(mockRtaEditarEmpleado);
            observer.complete();
        });
    }

    eliminarEmpleado(id: string) {
        return new Observable((observer: Observer<RtaApi<Empleado>>) => {
            observer.next(mockEliminarEmpleado);
            observer.complete();
        });
    }

    buscarPaginado(pageNumber: number) {
        return Observable.create((observer: Observer<RtaApi<Empleado>>) => {
            observer.next(mockBuscarPaginado);
            observer.complete();
        });
    }

    buscarEmpleadosPorFiltro(filtros: string) {
        return new Observable((observer: Observer<RtaApi<Empleado>>) => {
            observer.next(mockBuscarFiltrado);
            observer.complete();
        });
    }

    buscarEmpleadosPorID(id: string) {
        return new Observable((observer: Observer<RtaApi<Empleado>>) => {
            observer.next(mockRtaCrearEmpleado);
            observer.complete();
        });
    }
}