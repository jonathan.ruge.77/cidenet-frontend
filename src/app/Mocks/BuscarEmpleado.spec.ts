import { Empleado } from "@models/Empleado";
import { RtaApi } from "@models/RtaApi";

export const mockBuscarPaginado: RtaApi<Empleado> = {
    "bRta": true,
    "sMsg": "Lista encontrada",
    "listaPaginada": {
        "content": [
            {
                "codEmpleado": "25",
                "primerApellido": "RUGE",
                "segundoApellido": "CELIS",
                "primerNombre": "JONATHAN",
                "otrosNombres": "",
                "numeroIdentificacion": "c1020835716",
                "correoElectronico": "jonathan.ruge@cidenet.com.co",
                "fechaIngreso": "2021-10-05T05:00:00.000+00:00",
                "estado": "ACT",
                "fechaRegistro": "2021-10-15T22:33:32.000+00:00",
                "fechaModificacion": "2021-10-15T22:33:32.000+00:00",
                "codPais": {
                    "codPais": 1,
                    "nombre": "Colombia"
                },
                "codTipoIdentificacion": {
                    "codTipoIdentificacion": 1,
                    "nombre": "Cédula de Ciudadanía"
                },
                "codArea": {
                    "codArea": 3,
                    "nombre": "Compras"
                }
            },
            {
                "codEmpleado": "24",
                "primerApellido": "RODRIGUEZ",
                "segundoApellido": "SUAREZ",
                "primerNombre": "ALEJANDRA",
                "otrosNombres": "",
                "numeroIdentificacion": "1020859863",
                "correoElectronico": "alejandra.rodriguez@cidenet.com.us",
                "fechaIngreso": "2021-10-15T05:00:00.000+00:00",
                "estado": "ACT",
                "fechaRegistro": "2021-10-15T22:31:00.000+00:00",
                "fechaModificacion": "2021-10-15T22:33:32.000+00:00",
                "codPais": {
                    "codPais": 2,
                    "nombre": "Estados unidos"
                },
                "codTipoIdentificacion": {
                    "codTipoIdentificacion": 1,
                    "nombre": "Cédula de Ciudadanía"
                },
                "codArea": {
                    "codArea": 1,
                    "nombre": "Administración"
                }
            },
            {
                "codEmpleado": "23",
                "primerApellido": "RUGE",
                "segundoApellido": "CELIS",
                "primerNombre": "JONATHAN",
                "otrosNombres": "JAVIER",
                "numeroIdentificacion": "1020835716",
                "correoElectronico": "jonathan.ruge.23@cidenet.com.co",
                "fechaIngreso": "2021-10-15T05:00:00.000+00:00",
                "estado": "ACT",
                "fechaRegistro": "2021-10-15T22:29:56.000+00:00",
                "fechaModificacion": "2021-10-15T22:32:41.000+00:00",
                "codPais": {
                    "codPais": 1,
                    "nombre": "Colombia"
                },
                "codTipoIdentificacion": {
                    "codTipoIdentificacion": 2,
                    "nombre": "Cédula de Extranjería"
                },
                "codArea": {
                    "codArea": 8,
                    "nombre": "Otra"
                }
            }
        ],
        "pageable": {
            "sort": {
                "empty": false,
                "sorted": true,
                "unsorted": false
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 10,
            "paged": true,
            "unpaged": false
        },
        "totalPages": 1,
        "last": true,
        "totalElements": 3,
        "size": 10,
        "number": 0,
        "sort": {
            "empty": false,
            "sorted": true,
            "unsorted": false
        },
        "numberOfElements": 3,
        "first": true,
        "empty": false
    }
};

export const mockBuscarFiltrado: RtaApi<Empleado> = {
    "bRta": true,
    "sMsg": "",
    "lista": [
        {
            "codEmpleado": "23",
            "primerApellido": "RUGE",
            "segundoApellido": "CELIS",
            "primerNombre": "JONATHAN",
            "otrosNombres": "JAVIER",
            "numeroIdentificacion": "1020835716",
            "correoElectronico": "jonathan.ruge.23@cidenet.com.co",
            "fechaIngreso": "2021-10-15T05:00:00.000+00:00",
            "estado": "ACT",
            "fechaRegistro": "2021-10-15T22:29:56.000+00:00",
            "fechaModificacion": "2021-10-15T22:32:41.000+00:00",
            "codPais": {
                "codPais": 1,
                "nombre": "Colombia"
            },
            "codTipoIdentificacion": {
                "codTipoIdentificacion": 2,
                "nombre": "Cédula de Extranjería"
            },
            "codArea": {
                "codArea": 8,
                "nombre": "Otra"
            }
        },
        {
            "codEmpleado": "24",
            "primerApellido": "RODRIGUEZ",
            "segundoApellido": "SUAREZ",
            "primerNombre": "ALEJANDRA",
            "otrosNombres": "",
            "numeroIdentificacion": "1020859863",
            "correoElectronico": "alejandra.rodriguez@cidenet.com.us",
            "fechaIngreso": "2021-10-15T05:00:00.000+00:00",
            "estado": "ACT",
            "fechaRegistro": "2021-10-15T22:31:00.000+00:00",
            "fechaModificacion": "2021-10-15T22:33:32.000+00:00",
            "codPais": {
                "codPais": 2,
                "nombre": "Estados unidos"
            },
            "codTipoIdentificacion": {
                "codTipoIdentificacion": 1,
                "nombre": "Cédula de Ciudadanía"
            },
            "codArea": {
                "codArea": 1,
                "nombre": "Administración"
            }
        },
        {
            "codEmpleado": "25",
            "primerApellido": "RUGE",
            "segundoApellido": "CELIS",
            "primerNombre": "JONATHAN",
            "otrosNombres": "",
            "numeroIdentificacion": "c1020835716",
            "correoElectronico": "jonathan.ruge@cidenet.com.co",
            "fechaIngreso": "2021-10-05T05:00:00.000+00:00",
            "estado": "ACT",
            "fechaRegistro": "2021-10-15T22:33:32.000+00:00",
            "fechaModificacion": "2021-10-15T22:33:32.000+00:00",
            "codPais": {
                "codPais": 1,
                "nombre": "Colombia"
            },
            "codTipoIdentificacion": {
                "codTipoIdentificacion": 1,
                "nombre": "Cédula de Ciudadanía"
            },
            "codArea": {
                "codArea": 3,
                "nombre": "Compras"
            }
        }
    ]
}

export const mockBuscarPorId: RtaApi<Empleado> = {
    "bRta": true,
    "sMsg": "Lista encontrada",
    "lista": [
        {
            "codEmpleado": "23",
            "primerApellido": "RUGE",
            "segundoApellido": "CELIS",
            "primerNombre": "JONATHAN",
            "otrosNombres": "JAVIER",
            "numeroIdentificacion": "1020835716",
            "correoElectronico": "jonathan.ruge.23@cidenet.com.co",
            "fechaIngreso": "2021-10-15T05:00:00.000+00:00",
            "estado": "ACT",
            "fechaRegistro": "2021-10-15T22:29:56.000+00:00",
            "fechaModificacion": "2021-10-15T22:32:41.000+00:00",
            "codPais": {
                "codPais": 1,
                "nombre": "Colombia"
            },
            "codTipoIdentificacion": {
                "codTipoIdentificacion": 2,
                "nombre": "Cédula de Extranjería"
            },
            "codArea": {
                "codArea": 8,
                "nombre": "Otra"
            }
        }
    ]
}