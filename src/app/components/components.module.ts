import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { ComponentsRoutingModule } from './components-routing.module';


@NgModule({
  declarations: [
    SidebarComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    
  ],
  exports:[
    SidebarComponent,
    HeaderComponent
  ]
})
export class ComponentsModule { }
