
export interface Area {
    codArea: number;
    nombre: string;
}



export interface Pais {
    codPais: number;
    nombre: string;
}


export interface TipoIdentificacion {
    codTipoIdentificacion: number;
    nombre: string;
}