import { ListaPaginada } from "./Empleado";

export interface RtaApi<T>{
    bRta: boolean;
    sMsg: string;
    lista?: T[];
    listaPaginada?: ListaPaginada;
}