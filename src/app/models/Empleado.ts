import { Area, Pais, TipoIdentificacion } from "./Selects";

export interface ListaPaginada {
    content: Empleado[];
    pageable: Pageable;
    totalPages: number;
    totalElements: number;
    last: boolean;
    first: boolean;
    size: number;
    number: number;
    sort: Sort;
    numberOfElements: number;
    empty: boolean;
}

export interface Empleado {
    codEmpleado: string;
    primerApellido: string;
    segundoApellido: string;
    primerNombre: string;
    otrosNombres: string;
    numeroIdentificacion: string;
    correoElectronico: string;
    fechaIngreso: string;
    estado: string;
    fechaRegistro: string;
    fechaModificacion: string;
    codPais?: Pais;
    codTipoIdentificacion?: TipoIdentificacion;
    codArea?: Area;
}




export interface Pageable {
    sort: Sort;
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    unpaged: boolean;
}

export interface Sort {
    unsorted: boolean;
    empty: boolean;
    sorted: boolean;
}
