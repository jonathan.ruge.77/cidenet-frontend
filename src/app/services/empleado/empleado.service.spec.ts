import { mockBuscarPaginado, mockBuscarFiltrado, mockBuscarPorId } from '../../Mocks/BuscarEmpleado.spec';
import { mockBodyCrearEmpleado , mockRtaCrearEmpleado} from '../../Mocks/CrearEmpleado.spec';
import { of } from 'rxjs';
import { EmpleadoService } from './empleado.service';
import { mockBodyEditarEmpleado, mockRtaEditarEmpleado } from 'src/app/Mocks/EditarEmpleado.spec';
import { mockEliminarEmpleado } from 'src/app/Mocks/EliminarEmpleado.spec';

describe('EmpleadoService', () => {
    let service: EmpleadoService;
    let httpClientSpy: { post: jasmine.Spy, put:jasmine.Spy ,delete:jasmine.Spy,get:jasmine.Spy};

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['post','put','delete','get']);
        service = new EmpleadoService(httpClientSpy as any);
    })

    it('Creación servicio', () => {
        expect(service).toBeTruthy();
    });

    it('Servicio de crear el empleado', (done:DoneFn) => {
        httpClientSpy.post.and.returnValue(of(mockRtaCrearEmpleado));
        service.crearEmpleado(JSON.stringify(mockBodyCrearEmpleado)).subscribe(result => {
            expect(result).toEqual(mockRtaCrearEmpleado);
            done()
        });
    });

    it('Servicio de editar el empleado', (done:DoneFn) => {
        httpClientSpy.put.and.returnValue(of(mockRtaEditarEmpleado));
        service.editarEmpleado(JSON.stringify(mockBodyEditarEmpleado),"26").subscribe(result => {
            expect(result).toEqual(mockRtaEditarEmpleado);
            done()
        });
    });
    
    it('Servicio de eliminar el empleado', (done: DoneFn) => {
        httpClientSpy.delete.and.returnValue(of(mockEliminarEmpleado));
        service.eliminarEmpleado("26").subscribe(result => {
            expect(result).toEqual(mockEliminarEmpleado);
            done()
        });
    });
    
    it('Servicio de buscar por paginación', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockBuscarPaginado));
        service.buscarPaginado(0).subscribe(result => {
            expect(result).toEqual(mockBuscarPaginado);
            done()
        });
    });
    
    it('Servicio de buscar por filtro', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockBuscarFiltrado));
        service.buscarEmpleadosPorFiltro("primerNombre=J").subscribe(result => {
            expect(result).toEqual(mockBuscarFiltrado);
            done()
        });
    });
    
    it('Servicio de buscar por id', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockBuscarPorId));
        service.buscarEmpleadosPorID("23").subscribe(result => {
            expect(result).toEqual(mockBuscarPorId);
            done()
        });
    });
});
