import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environment/environment';
import { Empleado } from '@models/Empleado';
import { RtaApi } from '@models/RtaApi';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private _url = `${environment.url}/empleado`;
  private _headers = new HttpHeaders({ 'Content-Type': 'application/json','Authorization':'Basic Y2lkZW5ldDpDMUQzTjN0VDNzdDIwMjEh' });

  constructor(private _http: HttpClient) { }

  crearEmpleado(jsonEmpleado: string) {
    const url = `${this._url}/crearEmpleado`;
    return this._http.post<RtaApi<Empleado>>(url, jsonEmpleado, { headers: this._headers });
  }

  editarEmpleado(jsonEmpleado: string, id: string) {
    const url = `${this._url}/editarEmpleado/${id}`;
    return this._http.put<RtaApi<Empleado>>(url, jsonEmpleado, { headers: this._headers });
  }

  eliminarEmpleado(id: string) {
    const url = `${this._url}/eliminarEmpleado/${id}`;
    return this._http.delete<RtaApi<Empleado>>(url, { headers: this._headers });
  }

  buscarPaginado(pageNumber: number) {
    const url = `${this._url}/buscarEmpleados?pageSize=10&pageNumber=${pageNumber}&sort=fechaRegistro,desc`;
    return this._http.get<RtaApi<Empleado>>(url, { headers: this._headers });
  }

  buscarEmpleadosPorFiltro(filtros: string) {
    const url = `${this._url}/buscarEmpleadosPorFiltro?${filtros}`;
    return this._http.get<RtaApi<Empleado>>(url, { headers: this._headers });
  }

  buscarEmpleadosPorID(id: string) {
    const url = `${this._url}/buscarEmpleadoPorId?id=${id}`;
    return this._http.get<RtaApi<Empleado>>(url, { headers: this._headers });
  }
}
