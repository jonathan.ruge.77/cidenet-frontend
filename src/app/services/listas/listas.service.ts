import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environment/environment';
import { RtaApi } from '@models/RtaApi';
import { Area, Pais, TipoIdentificacion } from '@models/Selects';

@Injectable({
  providedIn: 'root'
})
export class ListasService {

  private _url = `${environment.url}/selects`;
  private _headers = new HttpHeaders({ 'Content-Type': 'application/json','Authorization':'Basic '+btoa('cidenet:C1D3N3tT3st2021!') });

  constructor(private _http: HttpClient) { }

  obtenerTiposIdentificacion() {
    const url = `${this._url}/listaTipoIdentificacion`;
    return this._http.get<RtaApi<TipoIdentificacion>>(url, { headers: this._headers });
  }

  obtenerPaises() {
    const url = `${this._url}/listaPaisEmpleo`;
    return this._http.get<RtaApi<Pais>>(url, { headers: this._headers });
  }
  
  obtenerAreas() {
    const url = `${this._url}/listaAreaEmpleado`;
    return this._http.get<RtaApi<Area>>(url, { headers: this._headers });
  }


}
