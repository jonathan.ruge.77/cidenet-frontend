import { ListasService } from './listas.service';
import { of } from 'rxjs';
import { mockSelectArea, mockSelectPais, mockSelectTipoIdent } from 'src/app/Mocks/Selects.spec';

describe('ListasService', () => {
    let service: ListasService;
    let httpClientSpy: { get: jasmine.Spy };

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        service = new ListasService(httpClientSpy as any);
    })

    it('Creación servicio', () => {
        expect(service).toBeTruthy();
    });

    it('Obtener tipos de identificación correctamente', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockSelectTipoIdent));
        service.obtenerTiposIdentificacion().subscribe(result => {
            expect(result).toEqual(mockSelectTipoIdent);
            done()
        })
    });

    it('Obtener lista de paises correctamente', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockSelectPais));
        service.obtenerPaises().subscribe(result => {
            expect(result).toEqual(mockSelectPais);
            done()
        })
    });
    
    it('Obtener lista de áreas correctamente', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockSelectArea));
        service.obtenerAreas().subscribe(result => {
            expect(result).toEqual(mockSelectArea);
            done()
        })
    });
})