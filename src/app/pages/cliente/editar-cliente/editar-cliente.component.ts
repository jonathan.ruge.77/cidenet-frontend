import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Empleado } from '@models/Empleado';
import { Area, Pais, TipoIdentificacion } from '@models/Selects';
import { EmpleadoService } from '@services/empleado/empleado.service';
import { ListasService } from '@services/listas/listas.service';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.scss']
})
export class EditarClienteComponent implements OnInit {

  public frmRegistro: FormGroup;
  public loading: boolean = false;
  public fechaMaxima: Date;
  public fechaMinima: Date;
  public empleadoActivo: Empleado = {
    codEmpleado:'',
    primerApellido:'',
    segundoApellido:'',
    primerNombre:'',
    otrosNombres:'',
    numeroIdentificacion:'',
    correoElectronico:'',
    fechaIngreso:'',
    estado:'',
    fechaRegistro:'',
    fechaModificacion:''
  };

  //Listas
  public listaTipoIdent: TipoIdentificacion[] = [];
  public listaArea: Area[] = [];
  public listaPais: Pais[] = [];

  constructor(
    private _listasService: ListasService,
    private _toastrService: ToastrService,
    private _empleadoService: EmpleadoService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date();
    this.fechaMinima.setMonth(this.fechaMinima.getMonth() - 1);
    this._route.params.subscribe(data => {
      this.empleadoActivo.codEmpleado = data['id'];
      this.obtenerEmpleado();
    })
    this.frmRegistro = this.inicializarFormulario();
  }

  ngOnInit(): void {
    this.obtenerTipoIdent();
    this.obtenerAreas();
    this.obtenerPais();
  }

  obtenerEmpleado() {
    this._empleadoService.buscarEmpleadosPorID(this.empleadoActivo.codEmpleado).subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.success(data.sMsg, 'Error');
          return;
        }
        this.empleadoActivo = data.lista![0];
        this.frmRegistro = this.inicializarFormularioConValores();
      },
      (err) => {
        this._toastrService.error('Ha ocurrido un error del servidor', 'Error');
        console.log(err);
        
      }
    )
  }

  obtenerTipoIdent() {
    this._listasService.obtenerTiposIdentificacion().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaTipoIdent = data.lista!;
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  obtenerAreas() {
    this._listasService.obtenerAreas().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaArea = data.lista!;
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  obtenerPais() {
    this._listasService.obtenerPaises().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaPais = data.lista!;
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  inicializarFormulario() {
    const patronAZ = Validators.pattern(/^[A-Z]*$/);
    const patronAZ2 = Validators.pattern(/^[A-Z ]*$/);
    const patronAZ3 = Validators.pattern(/^[a-zA-Z0-9]*$/);
    return new FormGroup({
      primerApellido: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
      segundoApellido: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
      primerNombre: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
      otrosNombres: new FormControl("", [Validators.maxLength(50), patronAZ2]),
      paisEmpleo: new FormControl("", [Validators.required]),
      tipoIdentificacion: new FormControl("", [Validators.required]),
      numeroIdentificacion: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ3]),
      fechaIngreso: new FormControl("", [Validators.required]),
      area: new FormControl("", [Validators.required]),
      estado: new FormControl("", [Validators.required]),
    })
  }
  
  inicializarFormularioConValores() {
    const patronAZ = Validators.pattern(/^[A-Z]*$/);
    const patronAZ2 = Validators.pattern(/^[A-Z ]*$/);
    const patronAZ3 = Validators.pattern(/^[a-zA-Z0-9]*$/);
    return new FormGroup({
      primerApellido: new FormControl(this.empleadoActivo.primerApellido, [Validators.required, Validators.maxLength(20), patronAZ]),
      segundoApellido: new FormControl(this.empleadoActivo.segundoApellido, [Validators.required, Validators.maxLength(20), patronAZ]),
      primerNombre: new FormControl(this.empleadoActivo.primerNombre, [Validators.required, Validators.maxLength(20), patronAZ]),
      otrosNombres: new FormControl(this.empleadoActivo.otrosNombres, [Validators.maxLength(50), patronAZ2]),
      paisEmpleo: new FormControl(this.empleadoActivo.codPais?.codPais, [Validators.required]),
      tipoIdentificacion: new FormControl(this.empleadoActivo.codTipoIdentificacion!.codTipoIdentificacion, [Validators.required]),
      numeroIdentificacion: new FormControl(this.empleadoActivo.numeroIdentificacion, [Validators.required, Validators.maxLength(20), patronAZ3]),
      fechaIngreso: new FormControl(this.empleadoActivo.fechaIngreso.split("T")[0], [Validators.required]),
      area: new FormControl(this.empleadoActivo.codArea!.codArea, [Validators.required]),
      estado: new FormControl(this.empleadoActivo.estado, [Validators.required]),
    })
  }

  actualizarEmpleado() {
    this.loading = true;
    this._empleadoService.editarEmpleado(JSON.stringify(this.frmRegistro.value), this.empleadoActivo.codEmpleado).subscribe(
      (data) => {
        this.loading = false;
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this._toastrService.success(data.sMsg, 'Correcto');
        this._router.navigate(['/inicio']);
      },
      (err) => {
        this.loading = false;
        console.log(err);
        this._toastrService.error('Ha ocurrido un error del servidor', 'Error');
      }
    )
    
    
  }

  convertirEnMayuscula(formControlName: string) {
    this.frmRegistro.get(formControlName)!.setValue(this.frmRegistro.get(formControlName)?.value.toUpperCase());
  }

  get primerApellido() { return this.frmRegistro.get('primerApellido'); }
  get segundoApellido() { return this.frmRegistro.get('segundoApellido'); }
  get primerNombre() { return this.frmRegistro.get('primerNombre'); }
  get otrosNombres() { return this.frmRegistro.get('otrosNombres'); }
  get paisEmpleo() { return this.frmRegistro.get('paisEmpleo'); }
  get tipoIdentificacion() { return this.frmRegistro.get('tipoIdentificacion'); }
  get numeroIdentificacion() { return this.frmRegistro.get('numeroIdentificacion'); }
  get fechaIngreso() { return this.frmRegistro.get('fechaIngreso'); }
  get area() { return this.frmRegistro.get('area'); }
  get estado() { return this.frmRegistro.get('estado'); }


}
