import { of, throwError } from 'rxjs';
import { ComponentsModule } from '@components/components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ListasService } from '@services/listas/listas.service';
import { EmpleadoService } from '@services/empleado/empleado.service';
import { RouterTestingModule } from '@angular/router/testing';
import { EditarClienteComponent } from './editar-cliente.component';
import { mockBuscarPorId } from 'src/app/Mocks/BuscarEmpleado.spec';
import { MockEmpleado } from 'src/app/Mocks/MockEmpleadoService.spec';
import { MockSelects } from 'src/app/Mocks/mockSelectsService.spec';
import { mockSelectArea, mockSelectPais, mockSelectTipoIdent } from 'src/app/Mocks/Selects.spec';
import { mockRtaEditarEmpleado } from 'src/app/Mocks/EditarEmpleado.spec';
import { IndexComponent } from '../index/index.component';

describe('EditarClienteComponent', () => {
    let component: EditarClienteComponent;
    let fixture: ComponentFixture<EditarClienteComponent>;
    let serviceEmpleado: EmpleadoService;
    let serviceSelects: ListasService;
    let toastTR: ToastrService;


    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [EditarClienteComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                ToastrModule.forRoot(),
                RouterTestingModule,
                BrowserAnimationsModule,
                ComponentsModule,
                RouterTestingModule.withRoutes(
                    [{path:'inicio', component:IndexComponent}]
                )
            ],
            providers: [
                { provide: EmpleadoService, useClass: MockEmpleado },
                { provide: ListasService, useClass: MockSelects },

            ]
        })
            .compileComponents();
        fixture = TestBed.createComponent(EditarClienteComponent);
        component = fixture.componentInstance;
        component.empleadoActivo = mockBuscarPorId.lista![0];
        fixture.detectChanges();
        serviceEmpleado = fixture.debugElement.injector.get(EmpleadoService);
        serviceSelects = fixture.debugElement.injector.get(ListasService);
        toastTR = fixture.debugElement.injector.get(ToastrService);
        toastTR.info = jasmine.createSpy();
        toastTR.error = jasmine.createSpy();
        toastTR.success = jasmine.createSpy();
    });



    it('Creación de componente', () => {
        expect(component).toBeTruthy();
    });

    it('Inicialización del formulario de registro', () => {
        const patronAZ = Validators.pattern(/^[A-Z]*$/);
        const patronAZ2 = Validators.pattern(/^[A-Z ]*$/);
        const patronAZ3 = Validators.pattern(/^[a-zA-Z0-9]*$/);
        const mockFormGroup = new FormGroup({
            primerApellido: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
            segundoApellido: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
            primerNombre: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
            otrosNombres: new FormControl("", [Validators.maxLength(50), patronAZ2]),
            paisEmpleo: new FormControl("", [Validators.required]),
            tipoIdentificacion: new FormControl("", [Validators.required]),
            numeroIdentificacion: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ3]),
            fechaIngreso: new FormControl("", [Validators.required]),
            area: new FormControl("", [Validators.required]),
            estado: new FormControl("", [Validators.required]),
        });

        expect(component.inicializarFormulario().value).toEqual(mockFormGroup.value);
    })

    it('Validar campos del formulario', () => {
        expect(component.primerApellido?.value).toBe("");
        expect(component.segundoApellido?.value).toBe("");
        expect(component.primerNombre?.value).toBe("");
        expect(component.otrosNombres?.value).toBe("");
        expect(component.paisEmpleo?.value).toBe("");
        expect(component.tipoIdentificacion?.value).toBe("");
        expect(component.numeroIdentificacion?.value).toBe("");
        expect(component.fechaIngreso?.value).toBe("");
        expect(component.area?.value).toBe("");
    })

    it('Cantidad de inputs en el formulario definidos correctamente', () => {
        const formElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro');
        const inputElements = formElement.querySelectorAll('input');
        expect(inputElements.length).toBe(6);
    });

    it('Cantidad de dropdowns en el formulario definidos correctamente', () => {
        const formElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro');
        const selectElements = formElement.querySelectorAll('select');
        expect(selectElements.length).toBe(4);
    });

    it('Validar valores iniciales del formulario', () => {
        const frmRegistro = component.frmRegistro;
        const frmRegistroValores = {
            primerApellido: '',
            segundoApellido: '',
            primerNombre: '',
            otrosNombres: '',
            paisEmpleo: '',
            tipoIdentificacion: '',
            numeroIdentificacion: '',
            fechaIngreso: '',
            area: '',
            estado: ''
        };
        expect(frmRegistro.value).toEqual(frmRegistroValores);
    });

    // Valida campo "Primer apellido"
    it('Validar primer apellido del formulario cuando es obligatorio', () => {
        const primerApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[0];
        const primerApellidoFrm = component.frmRegistro.get('primerApellido');
        expect(primerApellidoInput.value).toBe(primerApellidoFrm?.value);
        expect(primerApellidoFrm?.errors).not.toBeNull();
        expect(primerApellidoFrm?.errors?.required).toBeTruthy();
    });

    it('Validar primer apellido del formulario cuando tiene caracteres no permitidos', (done: DoneFn) => {
        const primerApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[0];
        primerApellidoInput.value = 'Ruge';
        primerApellidoInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const primerApellidoFrm = component.frmRegistro.get('primerApellido');
            expect(primerApellidoInput.value).toBe(primerApellidoFrm?.value);
            expect(primerApellidoFrm?.errors).not.toBeNull();
            expect(primerApellidoFrm?.errors?.pattern).toBeTruthy();
            done();
        })
    });

    it('Validar primer apellido del formulario cuando tiene mas de 20 caracteres', (done: DoneFn) => {
        const primerApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[0];
        primerApellidoInput.value = 'RUGECELISJONATHANJAVIER';
        primerApellidoInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const primerApellidoFrm = component.frmRegistro.get('primerApellido');
            expect(primerApellidoInput.value).toBe(primerApellidoFrm?.value);
            expect(primerApellidoFrm?.errors).not.toBeNull();
            expect(primerApellidoFrm?.errors?.maxlength).toBeTruthy();
            done();
        })
    });

    it('Validar primer apellido del formulario cuando esta bien', (done: DoneFn) => {
        const primerApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[0];
        primerApellidoInput.value = 'RUGE';
        primerApellidoInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const primerApellidoFrm = component.frmRegistro.get('primerApellido');
            expect(primerApellidoInput.value).toBe(primerApellidoFrm?.value);
            expect(primerApellidoFrm?.errors).toBeNull();
            done();
        })
    });

    // Valida campo "Segundo apellido"
    it('Validar segundo apellido del formulario cuando es obligatorio', (done: DoneFn) => {
        const segundoApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[1];
        const segundoApellidoFrm = component.frmRegistro.get('segundoApellido');
        expect(segundoApellidoInput.value).toEqual(segundoApellidoFrm?.value);
        expect(segundoApellidoFrm?.errors).not.toBeNull();
        expect(segundoApellidoFrm?.errors?.required).toBeTruthy();
        done();
    });

    it('Validar segundo apellido del formulario cuando tiene caracteres no permitidos', (done: DoneFn) => {
        const segundoApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[1];
        segundoApellidoInput.value = 'Ruge';
        segundoApellidoInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const segundoApellidoFrm = component.frmRegistro.get('segundoApellido');
            expect(segundoApellidoInput.value).toBe(segundoApellidoFrm?.value);
            expect(segundoApellidoFrm?.errors).not.toBeNull();
            expect(segundoApellidoFrm?.errors?.pattern).toBeTruthy();
            done();
        })
    });

    it('Validar segundo apellido del formulario cuando tiene mas de 20 caracteres', (done: DoneFn) => {
        const segundoApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[1];
        segundoApellidoInput.value = 'RUGECELISJONATHANJAVIER';
        segundoApellidoInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const segundoApellidoFrm = component.frmRegistro.get('segundoApellido');
            expect(segundoApellidoInput.value).toBe(segundoApellidoFrm?.value);
            expect(segundoApellidoFrm?.errors).not.toBeNull();
            expect(segundoApellidoFrm?.errors?.maxlength).toBeTruthy();
            done();

        })
    });

    it('Validar segundo apellido del formulario cuando esta bien', (done: DoneFn) => {
        const segundoApellidoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[1];
        segundoApellidoInput.value = 'RUGE';
        segundoApellidoInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const segundoApellidoFrm = component.frmRegistro.get('segundoApellido');
            expect(segundoApellidoInput.value).toBe(segundoApellidoFrm?.value);
            expect(segundoApellidoFrm?.errors).toBeNull();
            done();
        })
    });

    // Valida campo "Primer nombre"
    it('Validar primer Nombre del formulario cuando es obligatorio', () => {
        const primerNombreInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[2];
        const primerNombreFrm = component.frmRegistro.get('primerNombre');
        expect(primerNombreInput.value).toBe(primerNombreFrm?.value);
        expect(primerNombreFrm?.errors).not.toBeNull();
        expect(primerNombreFrm?.errors?.required).toBeTruthy();
    });

    it('Validar primer Nombre del formulario cuando tiene caracteres no permitidos', (done: DoneFn) => {
        const primerNombreInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[2];
        primerNombreInput.value = 'Ruge';
        primerNombreInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const primerNombreFrm = component.frmRegistro.get('primerNombre');
            expect(primerNombreInput.value).toBe(primerNombreFrm?.value);
            expect(primerNombreFrm?.errors).not.toBeNull();
            expect(primerNombreFrm?.errors?.pattern).toBeTruthy();
            done();
        })
    });

    it('Validar primer Nombre del formulario cuando tiene mas de 20 caracteres', (done: DoneFn) => {
        const primerNombreInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[2];
        primerNombreInput.value = 'RUGECELISJONATHANJAVIER';
        primerNombreInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const primerNombreFrm = component.frmRegistro.get('primerNombre');
            expect(primerNombreInput.value).toBe(primerNombreFrm?.value);
            expect(primerNombreFrm?.errors).not.toBeNull();
            expect(primerNombreFrm?.errors?.maxlength).toBeTruthy();
            done();
        })
    });

    it('Validar primer Nombre del formulario cuando esta bien', (done: DoneFn) => {
        const primerNombreInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[2];
        primerNombreInput.value = 'RUGE';
        primerNombreInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const primerNombreFrm = component.frmRegistro.get('primerNombre');
            expect(primerNombreInput.value).toBe(primerNombreFrm?.value);
            expect(primerNombreFrm?.errors).toBeNull();
            done();
        })
    });

    // Valida campo "Otros nombres"
    it('Validar otros nombres del formulario cuando tiene caracteres no permitidos', (done: DoneFn) => {
        const otrosNombresNombreInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[3];
        otrosNombresNombreInput.value = 'Ruge';
        otrosNombresNombreInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const otrosNombresNombreFrm = component.frmRegistro.get('otrosNombres');
            expect(otrosNombresNombreInput.value).toBe(otrosNombresNombreFrm?.value);
            expect(otrosNombresNombreFrm?.errors).not.toBeNull();
            expect(otrosNombresNombreFrm?.errors?.pattern).toBeTruthy();
            done();
        })
    });

    it('Validar otros nombres del formulario cuando tiene mas de 50 caracteres', (done: DoneFn) => {
        const otrosNombresNombreInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[3];
        otrosNombresNombreInput.value = 'RUGE CELIS JONATHAN JAVIER RUGE CELIS JONATHAN JAVIER';
        otrosNombresNombreInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const otrosNombresNombreFrm = component.frmRegistro.get('otrosNombres');
            expect(otrosNombresNombreInput.value).toBe(otrosNombresNombreFrm?.value);
            expect(otrosNombresNombreFrm?.errors).not.toBeNull();
            expect(otrosNombresNombreFrm?.errors?.maxlength).toBeTruthy();
            done();
        })
    });

    it('Validar otros nombres del formulario cuando esta bien', (done: DoneFn) => {
        const otrosNombresInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[3];
        otrosNombresInput.value = 'JAVIER PEREZ';
        otrosNombresInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const otrosNombresFrm = component.frmRegistro.get('otrosNombres');
            expect(otrosNombresInput.value).toEqual(otrosNombresFrm?.value);
            expect(otrosNombresFrm?.errors).toBeNull();
            done();
        })
    });

    // Valida campo "País empleo"
    it('Validar país del empleo del formulario cuando es obligatorio', () => {
        const paisEmpleoInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('select')[0];
        const paisEmpleoFrm = component.frmRegistro.get('paisEmpleo');
        expect(paisEmpleoInput.value).toEqual(paisEmpleoFrm?.value);
        expect(paisEmpleoFrm?.errors).not.toBeNull();
        expect(paisEmpleoFrm?.errors?.required).toBeTruthy();
    });

    // Valida campo "Tipo identificación"
    it('Validar tipo identificación del formulario cuando es obligatorio', (done: DoneFn) => {
        const tipoIdentificacionInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('select')[1];
        const tipoIdentificacionFrm = component.frmRegistro.get('tipoIdentificacion');
        expect(tipoIdentificacionInput.value).toEqual(tipoIdentificacionFrm?.value);
        expect(tipoIdentificacionFrm?.errors).not.toBeNull();
        expect(tipoIdentificacionFrm?.errors?.required).toBeTruthy();
        done();
    });

    // Valida campo "Tipo identificación"
    it('Validar número identificación del formulario cuando es obligatorio', () => {
        const numeroIdentificacionInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[4];
        const numeroIdentificacionFrm = component.frmRegistro.get('numeroIdentificacion');
        expect(numeroIdentificacionInput.value).toBe(numeroIdentificacionFrm?.value);
        expect(numeroIdentificacionFrm?.errors).not.toBeNull();
        expect(numeroIdentificacionFrm?.errors?.required).toBeTruthy();
    });

    it('Validar número identificación del formulario cuando tiene caracteres no permitidos', (done: DoneFn) => {
        const numeroIdentificacionInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[4];
        numeroIdentificacionInput.value = 'C1020 ';
        numeroIdentificacionInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const numeroIdentificacionFrm = component.frmRegistro.get('numeroIdentificacion');
            expect(numeroIdentificacionInput.value).toEqual(numeroIdentificacionFrm?.value);
            expect(numeroIdentificacionFrm?.errors).not.toBeNull();
            expect(numeroIdentificacionFrm?.errors?.pattern).toBeTruthy();
            done();
        })
    });

    it('Validar número identificación del formulario cuando tiene mas de 20 caracteres', (done: DoneFn) => {
        const numeroIdentificacionInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[4];
        numeroIdentificacionInput.value = 'cedulaDeCiudadania1020835716';
        numeroIdentificacionInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const numeroIdentificacionFrm = component.frmRegistro.get('numeroIdentificacion');
            expect(numeroIdentificacionInput.value).toEqual(numeroIdentificacionFrm?.value);
            expect(numeroIdentificacionFrm?.errors).not.toBeNull();
            expect(numeroIdentificacionFrm?.errors?.maxlength).toBeTruthy();
            done();
        })
    });

    it('Validar número identificación del formulario cuando esta bien', (done: DoneFn) => {
        const numeroIdentificacionInput: HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#frmRegistro').querySelectorAll('input')[4];
        numeroIdentificacionInput.value = 'C1020835716';
        numeroIdentificacionInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const numeroIdentificacionFrm = component.frmRegistro.get('numeroIdentificacion');
            expect(numeroIdentificacionInput.value).toEqual(numeroIdentificacionFrm?.value);
            expect(numeroIdentificacionFrm?.errors).toBeNull();
            done();
        })
    });

    //Test método convertir a mayuscula
    it('Probar función convertirAMayuscula de manera erronea', (done: DoneFn) => {
        let primerApellidoFrm = component.frmRegistro.get('primerApellido');
        primerApellidoFrm?.setValue('Ruge');
        component.convertirEnMayuscula('primerApellido');
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(primerApellidoFrm?.value).not.toEqual('Ruge');
            done();
        })
    })

    it('Probar función convertirAMayuscula de manera correcta', (done: DoneFn) => {
        let primerApellidoFrm = component.frmRegistro.get('primerApellido');
        primerApellidoFrm?.setValue('Ruge');
        component.convertirEnMayuscula('primerApellido');
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(primerApellidoFrm?.value).toEqual('RUGE');
            done();
        })
    })

    it("Validar el inicio del componente", () => {
        let spyObtenerTipoIdent = spyOn(component, 'obtenerTipoIdent');
        let spyobtenerAreas = spyOn(component, 'obtenerAreas');
        let spyobtenerPais = spyOn(component, 'obtenerPais');
        component.ngOnInit();
        expect(spyObtenerTipoIdent).toHaveBeenCalled();
        expect(spyobtenerAreas).toHaveBeenCalled();
        expect(spyobtenerPais).toHaveBeenCalled();
    });



    it("Validar que se llene la lista tipo identificación correctamente", () => {
        mockSelectTipoIdent.bRta = true;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerTiposIdentificacion").and.callFake(() => {
            return of(mockSelectTipoIdent);
        });
        component.obtenerTipoIdent();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it("Validar que se llene la lista tipo identificación incorrectamente", (done: DoneFn) => {
        mockSelectTipoIdent.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerTiposIdentificacion").and.callFake(() => {
            return of(mockSelectTipoIdent);
        });
        component.obtenerTipoIdent();
        fixture.detectChanges();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(false);
            done();
        });
    });

    it("Validar que se llene la lista tipo identificación error", () => {

        mockSelectTipoIdent.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerTiposIdentificacion").and.callFake(() => {
            return throwError(new Error('Error (TEST)'));
        });
        component.obtenerTipoIdent();
        spyObtenerSelect.call("").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error de servidor', 'Error');
        });
    });

    it("Validar que se llene la lista de paises", () => {
        mockSelectPais.bRta = true;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerPaises").and.callFake(() => {
            return of(mockSelectPais);
        });
        component.obtenerPais();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it("Validar que se llene la lista de paises incorrectamente", () => {
        mockSelectPais.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerPaises").and.callFake(() => {
            return of(mockSelectPais);
        });
        component.obtenerPais();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(false);
        });
    });

    it("Validar que se llene la lista paises error", () => {

        let spyObtenerSelect = spyOn(serviceSelects, "obtenerPaises").and.callFake(() => {
            return throwError(new Error('Error (TEST)'));
        });
        component.obtenerPais();
        spyObtenerSelect.call("").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error de servidor', 'Error');
        });
    });

    it("Validar que se llene la lista de areas correctamente", () => {
        mockSelectArea.bRta = true;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerAreas").and.callFake(() => {
            return of(mockSelectArea);
        });
        component.obtenerAreas();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it("Validar que se llene la lista de areas incorrectamente", () => {
        mockSelectArea.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerAreas").and.callFake(() => {
            return of(mockSelectArea);
        });
        component.obtenerAreas();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(false);
        });
    });

    it("Validar que se llene la lista areas error", () => {

        let spyObtenerSelect = spyOn(serviceSelects, "obtenerAreas").and.callFake(() => {
            return throwError(new Error('Error (TEST)'));
        });
        component.obtenerAreas();
        spyObtenerSelect.call("").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error de servidor', 'Error');
        });
    });

    it('Obtener empleado de manera correcta', () => {
        mockBuscarPorId.bRta = true;
        let spyObtenerEmpleado = spyOn(serviceEmpleado, 'buscarEmpleadosPorID').and.callFake(() => {
            return of(mockBuscarPorId);
        });
        component?.obtenerEmpleado();
        expect(spyObtenerEmpleado).toHaveBeenCalled();
        spyObtenerEmpleado.call("", "").subscribe(data => {
            expect(data.bRta).toEqual(true);
            expect(component.empleadoActivo.codEmpleado).toEqual(mockBuscarPorId.lista![0].codEmpleado);
        });
    });

    it('Obtener empleado de manera incorrecto', () => {
        mockBuscarPorId.bRta = false;
        let spyObtenerEmpleado = spyOn(serviceEmpleado, 'buscarEmpleadosPorID').and.callFake(() => {
            return of(mockBuscarPorId);
        });
        component?.obtenerEmpleado();
        expect(spyObtenerEmpleado).toHaveBeenCalled();
        spyObtenerEmpleado.call("", "").subscribe(data => {
            expect(data.bRta).toEqual(false);
        });
    });

    it("Obtener empleado de manera error", () => {
        let spyObtenerEmpleado = spyOn(serviceEmpleado, "buscarEmpleadosPorID").and.callFake(() => {
            return throwError(new Error('Error (TEST)'));
        });
        component.obtenerEmpleado();
        spyObtenerEmpleado.call("","").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error del servidor', 'Error');
        });
    });

    it("Validar llamar el relleno del formulario", () => {
        let spy = spyOn(component, 'inicializarFormularioConValores').and.callThrough();
        spyOn(component, 'obtenerEmpleado').and.callThrough();
        component.obtenerEmpleado();
        expect(spy).toBeDefined();
    });

    it('Actualizar empleado de manera correcta', () => {
        mockRtaEditarEmpleado.bRta = true;
        let spyEmpleado = spyOn(serviceEmpleado, 'editarEmpleado').and.callFake(() => {
            return of(mockRtaEditarEmpleado);
        });
        component?.actualizarEmpleado();
        expect(spyEmpleado).toHaveBeenCalled();
        spyEmpleado.call("", "","").subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it('Actualizar empleado de manera incorrecta', () => {

        mockRtaEditarEmpleado.bRta = false;
        let spyEmpleado = spyOn(serviceEmpleado, 'editarEmpleado').and.callFake(() => {
            return of(mockRtaEditarEmpleado);
        });
        component?.actualizarEmpleado();
        expect(spyEmpleado).toHaveBeenCalled();
        spyEmpleado.call("", "","").subscribe(data => {
            expect(data.bRta).toEqual(false);
            expect(toastTR.error).toHaveBeenCalled();
        });
    });

    it("Actualizar empleado de manera error", () => {

        let spyObtenerEmpleado = spyOn(serviceEmpleado, "editarEmpleado").and.callFake(() => {
            return throwError(new Error('Error (TEST)'));
        });
        component.actualizarEmpleado();
        spyObtenerEmpleado.call("","","").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error del servidor', 'Error');
        });
    });


});
