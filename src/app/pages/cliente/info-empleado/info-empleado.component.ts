import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Empleado } from '@models/Empleado';
import { Area, Pais, TipoIdentificacion } from '@models/Selects';
import { EmpleadoService } from '@services/empleado/empleado.service';
import { ListasService } from '@services/listas/listas.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-info-empleado',
  templateUrl: './info-empleado.component.html',
  styleUrls: ['./info-empleado.component.scss']
})
export class InfoEmpleadoComponent implements OnInit {

  public empleadoActivo: Empleado = {
    codEmpleado: '',
    primerApellido: '',
    segundoApellido: '',
    primerNombre: '',
    otrosNombres: '',
    numeroIdentificacion: '',
    correoElectronico: '',
    fechaIngreso: '',
    estado: '',
    fechaRegistro: '',
    fechaModificacion: ''
  };

  public codPais: string = "";
  public codTipoIdent: string = "";
  public codArea: string = "";

  constructor(
    private _toastrService: ToastrService,
    private _empleadoService: EmpleadoService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._route.params.subscribe(data => {
      this.empleadoActivo.codEmpleado = data['id'];
      this.obtenerEmpleado();
    })
  }

  obtenerEmpleado() {
    this._empleadoService.buscarEmpleadosPorID(this.empleadoActivo.codEmpleado).subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.empleadoActivo = data.lista![0];
        this.empleadoActivo.fechaIngreso = this.empleadoActivo.fechaIngreso.split("T")[0];
        this.empleadoActivo.fechaRegistro = this.cambiarFormatoFecha(this.empleadoActivo.fechaRegistro);
        this.empleadoActivo.fechaModificacion = this.cambiarFormatoFecha(this.empleadoActivo.fechaModificacion);

        this.empleadoActivo.estado = this.empleadoActivo.estado == 'ACT' ? 'ACTIVO' : 'INACTIVO';
        this.codArea = this.empleadoActivo.codArea!.nombre;
        this.codTipoIdent = this.empleadoActivo.codTipoIdentificacion!.nombre;
        this.codPais = this.empleadoActivo.codPais!.nombre;
      },
      (err) => {
        this._toastrService.error('Ha ocurrido un error del servidor', 'Error');
        console.log(err);

      }
    )
  }

  cambiarFormatoFecha(fechaActual: string): string {

    try {
      if (fechaActual == "" || fechaActual == null) {
        return "";
      }
      var p = fechaActual.split("T")[0].split(/\D/g);
      var p2 = fechaActual.split("T")[1].split(".")[0];

      return [p[2], p[1], p[0]].join("/") + " " + p2
    } catch (error) {
      return "";
    }
  }

}
