import { delay } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
import { ComponentsModule } from '@components/components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmpleadoService } from '@services/empleado/empleado.service';
import { RouterTestingModule } from '@angular/router/testing';
import { InfoEmpleadoComponent } from './info-empleado.component';
import { MockEmpleado } from 'src/app/Mocks/MockEmpleadoService.spec';
import { mockBuscarPorId } from 'src/app/Mocks/BuscarEmpleado.spec';

describe('InfoEmpleadoComponent', () => {
    let component: InfoEmpleadoComponent;
    let fixture: ComponentFixture<InfoEmpleadoComponent>;
    let service: EmpleadoService;
    let toastTR: ToastrService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [InfoEmpleadoComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                ToastrModule.forRoot(),
                RouterTestingModule,
                BrowserAnimationsModule,
                ComponentsModule
            ],
            providers: [

                { provide: EmpleadoService, useClass: MockEmpleado },
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(InfoEmpleadoComponent);
        component = fixture.componentInstance;
        service = fixture.debugElement.injector.get(EmpleadoService);
        toastTR = fixture.debugElement.injector.get(ToastrService);
        fixture.detectChanges();
    });

    it('Crear componente', () => {
        expect(component).toBeTruthy();
    });

    it('Obtener empleado de manera correcta', () => {
        mockBuscarPorId.bRta = true;
        let spyObtenerEmpleado = spyOn(service, 'buscarEmpleadosPorID').and.callFake(() => {
            return of(mockBuscarPorId);
        });
        component?.obtenerEmpleado();
        expect(spyObtenerEmpleado).toHaveBeenCalled();
        spyObtenerEmpleado.call("", "").subscribe(data => {
            expect(data.bRta).toEqual(true);
            expect(component.empleadoActivo.codEmpleado).toEqual(mockBuscarPorId.lista![0].codEmpleado);
        });
    });

    it('Obtener empleado de manera incorrecto', () => {
        toastTR.info = jasmine.createSpy();
        toastTR.error = jasmine.createSpy();
        toastTR.success = jasmine.createSpy();
        mockBuscarPorId.bRta = false;
        let spyObtenerEmpleado = spyOn(service, 'buscarEmpleadosPorID').and.callFake(() => {
            return of(mockBuscarPorId);
        });
        component?.obtenerEmpleado();
        expect(spyObtenerEmpleado).toHaveBeenCalled();
        spyObtenerEmpleado.call("", "").subscribe(data => {
            expect(data.bRta).toEqual(false);
            expect(toastTR.error).toHaveBeenCalled();
        });
    });

    it("Obtener empleado de manera error", () => {
        let spyObtenerEmpleado = spyOn(service, "editarEmpleado").and.callFake(() => {
            return throwError(new Error('Error (TEST)'));
        });
        component.obtenerEmpleado();
        spyObtenerEmpleado.call("","","").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error del servidor', 'Error');
        });
    });

    it('Probar cambiar formato fecha correctamente', () => {
        const expectRta = "15/10/2021 05:00:00";
        const valorFecha = component!.cambiarFormatoFecha("2021-10-15T05:00:00.000+00:00");
        expect(valorFecha).toEqual(expectRta);
    })

    it('Probar cambiar formato fecha correctamente', () => {
        const expectRta = "15-10-202105:00:00";
        const valorFecha = component!.cambiarFormatoFecha("2021-10-15T05:00:00.000+00:00");
        expect(valorFecha).not.toEqual(expectRta);
    })
});
