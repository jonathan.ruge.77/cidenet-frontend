import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Empleado } from '@models/Empleado';
import { Area, Pais, TipoIdentificacion } from '@models/Selects';
import { EmpleadoService } from '@services/empleado/empleado.service';
import { ListasService } from '@services/listas/listas.service';
import { ToastrService } from "ngx-toastr";
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  //Listas
  public listaTipoIdent: TipoIdentificacion[] = [];
  public listaPais: Pais[] = [];
  public listaEmpleados: Empleado[] = [];

  public cantidadPaginas: number[] = [];
  public paginaActiva: number = 0;

  public frmFiltro = {
    primerNombre: '',
    otrosNombres: '',
    primerApellido: '',
    segundoApellido: '',
    codTipoIdentificacion: '',
    numeroIdentificacion: '',
    codPais: '',
    correoElectronico: '',
    estado: '',
  };

  public loading: boolean = false;
  constructor(
    private _listasService: ListasService,
    private _toastrService: ToastrService,
    private _empleadoService: EmpleadoService,
    private _router: Router,
  ) {
  }

  ngOnInit(): void {
    this.obtenerTipoIdent();
    this.obtenerPais();
    this.obtenerEmpleados(this.paginaActiva);
  }

  obtenerTipoIdent() {
    this._listasService.obtenerTiposIdentificacion().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaTipoIdent = data.lista!;
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  obtenerPais() {
    this._listasService.obtenerPaises().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaPais = data.lista!;
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  obtenerEmpleados(pagina: number) {
    this.loading = true;
    this._empleadoService.buscarPaginado(pagina).subscribe(
      (data) => {
        this.loading = false;
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaEmpleados = data.listaPaginada!.content;
        for (let i = 0; i < data.listaPaginada!.totalPages; i++) {
          this.cantidadPaginas[i] = i;
        }
        
      },
      (err) => {
        this.loading = false;
        console.log(err);
        this._toastrService.error('Error del servidor', 'Error');
      }
    )
  }

  cambiarPagina(pagina: number) {
    this.paginaActiva = pagina;
    this.obtenerEmpleados(pagina);
  }

  anteriorTabla() {
    if (this.paginaActiva == 0) {
      return;
    }
    this.cambiarPagina(this.paginaActiva - 1);
  }

  siguienteTabla() {
    if (this.paginaActiva == this.cantidadPaginas[this.cantidadPaginas.length - 1]) {
      return;
    }
    this.cambiarPagina(this.paginaActiva + 1);
  }



  escribirFiltro() {
    let parametrosBusqueda = "";
    if (this.frmFiltro.primerNombre != "") parametrosBusqueda += `primerNombre=${this.frmFiltro.primerNombre}&`;
    if (this.frmFiltro.otrosNombres != "") parametrosBusqueda += `otrosNombres=${this.frmFiltro.otrosNombres}&`;
    if (this.frmFiltro.primerApellido != "") parametrosBusqueda += `primerApellido=${this.frmFiltro.primerApellido}&`;
    if (this.frmFiltro.segundoApellido != "") parametrosBusqueda += `segundoApellido=${this.frmFiltro.segundoApellido}&`;
    if (this.frmFiltro.codTipoIdentificacion != "") parametrosBusqueda += `codTipoIdentificacion=${this.frmFiltro.codTipoIdentificacion}&`;
    if (this.frmFiltro.numeroIdentificacion != "") parametrosBusqueda += `numeroIdentificacion=${this.frmFiltro.numeroIdentificacion}&`;
    if (this.frmFiltro.codPais != "") parametrosBusqueda += `codPais=${this.frmFiltro.codPais}&`;
    if (this.frmFiltro.correoElectronico != "") parametrosBusqueda += `correoElectronico=${this.frmFiltro.correoElectronico}&`;
    if (this.frmFiltro.estado != "") parametrosBusqueda += `estado=${this.frmFiltro.estado}&`;

    if (parametrosBusqueda == "") {
      this.obtenerEmpleados(0);
      return;
    }

    this.loading = true;
    this._empleadoService.buscarEmpleadosPorFiltro(parametrosBusqueda).subscribe(
      (data) => {
        this.loading = false;
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaEmpleados = data.lista!;
      },
      (err) => {
        this.loading = false;
        this._toastrService.error('Ha ocurrido un error interno', 'Error');
        console.log(err);
      }
    )

  }

  eliminarEmpleado(id:string) {
    document.getElementById("modal-" + id)!.click();
    this._empleadoService.eliminarEmpleado(id).subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this._toastrService.success(data.sMsg, 'Correcto');
        this.obtenerEmpleados(0);
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error interno', 'Error');
      }
    )
  }


}
