import { mockBuscarFiltrado, mockBuscarPaginado } from './../../../Mocks/BuscarEmpleado.spec';
import { of, throwError } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { EmpleadoService } from '@services/empleado/empleado.service';
import { ListasService } from '@services/listas/listas.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { MockEmpleado } from 'src/app/Mocks/MockEmpleadoService.spec';
import { MockSelects } from 'src/app/Mocks/mockSelectsService.spec';
import { mockSelectPais, mockSelectTipoIdent } from 'src/app/Mocks/Selects.spec';

import { IndexComponent } from './index.component';
import { mockEliminarEmpleado } from 'src/app/Mocks/EliminarEmpleado.spec';

describe('IndexComponent', () => {
    let component: IndexComponent;
    let fixture: ComponentFixture<IndexComponent>;
    let serviceEmpleado: EmpleadoService;
    let serviceSelects: ListasService;
    let toastTR: ToastrService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [IndexComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                ToastrModule.forRoot(),
                BrowserAnimationsModule,
                RouterTestingModule
            ],
            providers: [
                { provide: EmpleadoService, useClass: MockEmpleado },
                { provide: ToastrService },
                { provide: ListasService, useClass: MockSelects },
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(IndexComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        serviceEmpleado = fixture.debugElement.injector.get(EmpleadoService);
        serviceSelects = fixture.debugElement.injector.get(ListasService);
        toastTR = fixture.debugElement.injector.get(ToastrService);
        toastTR.info = jasmine.createSpy();
        toastTR.error = jasmine.createSpy();
        toastTR.success = jasmine.createSpy();
    });

    it('Index: Creación de componente', () => {
        expect(component).toBeTruthy();
    });

    it("Validar el inicio del componente", () => {
        let spyObtenerTipoIdent = spyOn(component, 'obtenerTipoIdent');
        let spyobtenerPais = spyOn(component, 'obtenerPais');
        component.ngOnInit();
        expect(spyObtenerTipoIdent).toHaveBeenCalled();
        expect(spyobtenerPais).toHaveBeenCalled();
    });



    it("Validar que se llene la lista tipo identificación correctamente", () => {
        mockSelectTipoIdent.bRta = true;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerTiposIdentificacion").and.callFake(() => {
            return of(mockSelectTipoIdent);
        });
        component.obtenerTipoIdent();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it("Validar que se llene la lista tipo identificación incorrectamente", (done: DoneFn) => {
        mockSelectTipoIdent.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerTiposIdentificacion").and.callFake(() => {
            return of(mockSelectTipoIdent);
        });
        component.obtenerTipoIdent();
        fixture.detectChanges();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(false);
            done();
        });
    });

    it("Validar que se llene la lista tipo identificación error", () => {

        mockSelectTipoIdent.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerTiposIdentificacion").and.callFake(() => {
            return throwError(new Error('Eror'));
        });
        component.obtenerTipoIdent();
        spyObtenerSelect.call("").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error de servidor', 'Error');
        });
    });

    it("Validar que se llene la lista de paises", () => {
        mockSelectPais.bRta = true;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerPaises").and.callFake(() => {
            return of(mockSelectPais);
        });
        component.obtenerPais();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it("Validar que se llene la lista de paises incorrectamente", () => {
        mockSelectPais.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerPaises").and.callFake(() => {
            return of(mockSelectPais);
        });
        component.obtenerPais();
        spyObtenerSelect.call("").subscribe(data => {
            expect(data.bRta).toEqual(false);
        });
    });

    it("Validar que se llene la lista de paises error", () => {

        mockSelectPais.bRta = false;
        let spyObtenerSelect = spyOn(serviceSelects, "obtenerPaises").and.callFake(() => {
            return throwError(new Error('Eror'));
        });
        component.obtenerPais();
        spyObtenerSelect.call("").subscribe(data => {
        }, (err) => {
            expect(toastTR.error).toHaveBeenCalledWith('Ha ocurrido un error de servidor', 'Error');
        });
    });

    it("Obtener empleados paginados de manera correcta", () => {
        component.paginaActiva = 0;
        fixture.detectChanges();
        mockBuscarPaginado.bRta = true;
        let spyObtenerEmpleados = spyOn(serviceEmpleado, "buscarPaginado").and.callFake(() => {
            return of(mockBuscarPaginado);
        });
        component.obtenerEmpleados(component.paginaActiva);
        spyObtenerEmpleados.call("", component.paginaActiva).subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it("Obtener empleados paginados de manera incorrecta", () => {
        component.paginaActiva = 0;
        fixture.detectChanges();
        mockBuscarPaginado.bRta = false;
        let spyObtenerEmpleados = spyOn(serviceEmpleado, "buscarPaginado").and.callFake(() => {
            return of(mockBuscarPaginado);
        });
        component.obtenerEmpleados(component.paginaActiva);
        spyObtenerEmpleados.call("", component.paginaActiva).subscribe(data => {
            expect(component.loading).toEqual(false);
            expect(data.bRta).toEqual(false);
        });
    });

    it("Obtener empleados paginados de manera error", () => {
        component.paginaActiva = 0;
        fixture.detectChanges();
        let spyObtenerSelect = spyOn(serviceEmpleado, "buscarPaginado").and.callFake(() => {
            return throwError(new Error('Error'));
        });
        component.obtenerEmpleados(component.paginaActiva);
        spyObtenerSelect.call("", component.paginaActiva).subscribe(data => {
        }, (err) => {
            expect(component.loading).toEqual(false);
            expect(toastTR.error).toHaveBeenCalledWith('Error del servidor', 'Error');
        });
    });

    it("Cambiar pagina", () => {
        component.paginaActiva = 0;
        fixture.detectChanges();
        component.cambiarPagina(2);
        expect(component.paginaActiva).toEqual(2);
    });

    it("Cambiar pagina hacia adelante", () => {
        component.paginaActiva = 0;
        component.cantidadPaginas = [0, 1, 2, 3];
        fixture.detectChanges();
        component.siguienteTabla();
        expect(component.paginaActiva).toEqual(1);
    });

    it("Cambiar pagina hacia atras", () => {
        component.paginaActiva = 2;
        fixture.detectChanges();
        component.anteriorTabla();
        expect(component.paginaActiva).toEqual(1);
    });

    it("Escribir filtro de manera correcta", () => {

        mockBuscarFiltrado.bRta = true;
        let spyObtenerEmpleados = spyOn(serviceEmpleado, "buscarEmpleadosPorFiltro").and.callFake(() => {
            return of(mockBuscarFiltrado);
        });
        component.escribirFiltro();
        spyObtenerEmpleados.call("", "").subscribe(data => {
            expect(data.bRta).toEqual(true);
        });
    });

    it("Escribir filtro de manera incorrecta", () => {

        mockBuscarFiltrado.bRta = false;
        let spyObtenerEmpleados = spyOn(serviceEmpleado, "buscarEmpleadosPorFiltro").and.callFake(() => {
            return of(mockBuscarFiltrado);
        });
        component.escribirFiltro();
        spyObtenerEmpleados.call("", "").subscribe(data => {
            expect(data.bRta).toEqual(false);
            expect(component.loading).toEqual(false);
        });
    });

    it("Escribir filtro de manera error", () => {

        let spyObtenerEmpleados = spyOn(serviceEmpleado, "buscarEmpleadosPorFiltro").and.callFake(() => {
            return throwError(new Error('Error'));
        });
        component.escribirFiltro();
        spyObtenerEmpleados.call("", "").subscribe(data => {
        }, (err) => {
            expect(component.loading).toEqual(false);
        });
    });

    it("Eliminar empleado de manera correcta", (done:DoneFn) => {
        component.listaEmpleados = mockBuscarPaginado.listaPaginada!.content;
        component.loading = false;
        fixture.detectChanges();
        let btnModal = fixture.debugElement.nativeElement.querySelector("#modal-25");        
        btnModal.click();
        fixture.detectChanges();
        fixture.whenStable().then(() => {            
            mockEliminarEmpleado.bRta = true;
            let spyEliminar = spyOn(serviceEmpleado, "eliminarEmpleado").and.callFake(() => {
                return of(mockEliminarEmpleado);
            });
            component.eliminarEmpleado("25");
            spyEliminar.call("", "25").subscribe(data => {
                expect(data.bRta).toEqual(true);
                expect(component.loading).toEqual(false);
            });
            done();
        })
    })

    it("Eliminar empleado de manera incorrecta", (done:DoneFn) => {
        component.listaEmpleados = mockBuscarPaginado.listaPaginada!.content;
        component.loading = false;
        fixture.detectChanges();
        let btnModal = fixture.debugElement.nativeElement.querySelector("#modal-23");
        btnModal.click();
        fixture.detectChanges();
        fixture.whenStable().then(() => {            
            mockEliminarEmpleado.bRta = false;
            let spyEliminar = spyOn(serviceEmpleado, "eliminarEmpleado").and.callFake(() => {
                return of(mockEliminarEmpleado);
            });
            component.eliminarEmpleado("23");
            spyEliminar.call("", "23").subscribe(data => {
                expect(data.bRta).toEqual(false);
                expect(component.loading).toEqual(false);
            });
            done();
        })
    })

    it("Eliminar empleado de manera error", (done:DoneFn) => {
        component.listaEmpleados = mockBuscarPaginado.listaPaginada!.content;
        component.loading = false;
        fixture.detectChanges();
        let btnModal = fixture.debugElement.nativeElement.querySelector("#modal-24");
        btnModal.click();
        fixture.detectChanges();
        fixture.whenStable().then(() => {            
            mockEliminarEmpleado.bRta = false;
            let spyEliminar = spyOn(serviceEmpleado, "eliminarEmpleado").and.callFake(() => {
                return throwError(new Error('Error'));
            });
            component.eliminarEmpleado("24");
            spyEliminar.call("", "24").subscribe(data => {
            }, (err) => {
                expect(component.loading).toEqual(false);
            });
            done();
        })
    })

});
