import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarClienteComponent } from './editar-cliente/editar-cliente.component';
import { IndexComponent } from './index/index.component';
import { InfoEmpleadoComponent } from './info-empleado/info-empleado.component';
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  { path: 'inicio', component: IndexComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'inicio/editar/:id', component: EditarClienteComponent },
  { path: 'inicio/info/:id', component: InfoEmpleadoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
