import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClienteRoutingModule } from './cliente-routing.module';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { RegistroComponent } from './registro/registro.component';
import { IndexComponent } from './index/index.component';
import { EditarClienteComponent } from './editar-cliente/editar-cliente.component';
import { ComponentsModule } from '@components/components.module';
import { InfoEmpleadoComponent } from './info-empleado/info-empleado.component';


@NgModule({
  declarations: [
    RegistroComponent,
    IndexComponent,
    EditarClienteComponent,
    InfoEmpleadoComponent
  ],
  imports: [
    CommonModule,
    ClienteRoutingModule,
    ToastrModule.forRoot(),
    ComponentsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    ToastrService
  ]
})
export class ClienteModule { }
