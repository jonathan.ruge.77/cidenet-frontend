import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Area, Pais, TipoIdentificacion } from '@models/Selects';
import { EmpleadoService } from '@services/empleado/empleado.service';
import { ListasService } from '@services/listas/listas.service';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  public frmRegistro: FormGroup;
  public loading: boolean = false;
  public fechaMaxima: Date;
  public fechaMinima: Date;

  //Listas
  public listaTipoIdent: TipoIdentificacion[] = [];
  public listaArea: Area[] = [];
  public listaPais: Pais[] = [];

  constructor(
    private _listasService: ListasService,
    private _toastrService: ToastrService,
    private _empleadoService: EmpleadoService,
    private _router: Router,
  ) {
    this.frmRegistro = this.inicializarFormulario();
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date();
    this.fechaMinima.setMonth(this.fechaMinima.getMonth() - 1);
  }

  ngOnInit(): void {
    this.obtenerTipoIdent();
    this.obtenerAreas();
    this.obtenerPais();
  }

  obtenerTipoIdent() {
    this._listasService.obtenerTiposIdentificacion().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaTipoIdent = data.lista!;
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  obtenerAreas() {
    this._listasService.obtenerAreas().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaArea = data.lista!;
      },
      (err: any) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  obtenerPais() {
    this._listasService.obtenerPaises().subscribe(
      (data) => {
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this.listaPais = data.lista!;
      },
      (err) => {
        console.log(err);
        this._toastrService.error('Ha ocurrido un error de servidor', 'Error');
      }
    )
  }

  inicializarFormulario() {
    const patronAZ = Validators.pattern(/^[A-Z]*$/);
    const patronAZ2 = Validators.pattern(/^[A-Z ]*$/);
    const patronAZ3 = Validators.pattern(/^[a-zA-Z0-9]*$/);
    return new FormGroup({
      primerApellido: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
      segundoApellido: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
      primerNombre: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ]),
      otrosNombres: new FormControl("", [Validators.maxLength(50), patronAZ2]),
      paisEmpleo: new FormControl("", [Validators.required]),
      tipoIdentificacion: new FormControl("", [Validators.required]),
      numeroIdentificacion: new FormControl("", [Validators.required, Validators.maxLength(20), patronAZ3]),
      fechaIngreso: new FormControl("", [Validators.required]),
      area: new FormControl("", [Validators.required]),
    })
  }

  registrarEmpleado() {
    this.loading = true;
    this._empleadoService.crearEmpleado(JSON.stringify(this.frmRegistro.value)).subscribe(
      (data) => {
        this.loading = false;
        if (!data.bRta) {
          this._toastrService.error(data.sMsg, 'Error');
          return;
        }
        this._toastrService.success(data.sMsg, 'Correcto');
        this._router.navigate(['/inicio']);
      },
      (err) => {
        this.loading = false;
        console.log(err);
        this._toastrService.error('Ha ocurrido un error del servidor', 'Error');
      }
    )
    
    
  }

  convertirEnMayuscula(formControlName: string) {
    this.frmRegistro.get(formControlName)!.setValue(this.frmRegistro.get(formControlName)?.value.toUpperCase());
  }

  get primerApellido() { return this.frmRegistro.get('primerApellido'); }
  get segundoApellido() { return this.frmRegistro.get('segundoApellido'); }
  get primerNombre() { return this.frmRegistro.get('primerNombre'); }
  get otrosNombres() { return this.frmRegistro.get('otrosNombres'); }
  get paisEmpleo() { return this.frmRegistro.get('paisEmpleo'); }
  get tipoIdentificacion() { return this.frmRegistro.get('tipoIdentificacion'); }
  get numeroIdentificacion() { return this.frmRegistro.get('numeroIdentificacion'); }
  get fechaIngreso() { return this.frmRegistro.get('fechaIngreso'); }
  get area() { return this.frmRegistro.get('area'); }


}
