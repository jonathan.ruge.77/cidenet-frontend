# Cidenet

Proyecto creado con [Angular CLI](https://github.com/angular/angular-cli) en la versión 11.2.11. Es el proyecto Frontend para la prueba técnica de Cidenet.

## Ejecutar pruebas unitarias

Para ejecutar las pruebas, ejecutar el comando `npm run test`, se realizan via [Karma](https://karma-runner.github.io), se puede visualizar un informe completo despues de ejecutar las pruebas en la carpeta generada en la raíz del proyecto: coverage/cidenet/index.html

## Correr el proyecto

Ejecutar `npm start` para ejecutar el servidor de desarrollo. Navegar al link `http://localhost:4200/`. La aplicación cargará automaticamente el inicio.
